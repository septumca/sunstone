extends Interaction

@export var is_on_overworld: bool = true
@export_file("*.tscn") var scene_path: String

signal triggered

func trigger(p: Player) -> void:
	if scene_path == null:
		print("Scene path is empty to change to!")
		return

	triggered.emit()
	if is_on_overworld:
		SceneSwitch.goto_event_scene(scene_path)
	else:
		SceneSwitch.goto_overworld()
