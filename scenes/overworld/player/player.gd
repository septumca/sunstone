extends CharacterBody2D

class_name Player

enum PlayerState { IDLE, MOVING }

@onready var animation_player: AnimationPlayer = $AnimationPlayer

@export var speed: float = 100

var state: PlayerState = PlayerState.IDLE
var interaction: Interaction = null

func update_State() -> void:
	var is_moving = velocity.length_squared() > 0
	match state:
		PlayerState.MOVING:
			if !is_moving:
				state = PlayerState.IDLE
				animation_player.stop()
		PlayerState.IDLE:
			if is_moving:
				state = PlayerState.MOVING
				animation_player.play("walk")

func _unhandled_input(event):
	if event.is_action_released("action") and interaction != null:
		interaction.trigger(self)

func _physics_process(delta):
	var input_dir = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = input_dir * speed
	update_State()
	move_and_slide()

func set_interaction(i: Interaction) -> void:
	interaction = i

func clear_interaction(i: Interaction) -> void:
	if interaction == i:
		interaction = null
