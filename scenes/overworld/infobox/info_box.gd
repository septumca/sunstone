extends Panel

class_name InfoBox

@onready var text: Label = $Text

func set_text(t: String) -> void:
	text.text = t
