extends Area2D

class_name PointOfInterest

@export var msg: String = ""
@export var interaction: Interaction

@onready var info_box: InfoBox = $InfoBox

func _ready() -> void:
	info_box.set_text(msg)

func _on_body_entered(body: Player) -> void:
	if interaction == null:
		print("player entered but interaction is null")
		return
	info_box.visible = true
	body.set_interaction(interaction)

func _on_body_exited(body: Player) -> void:
	if interaction == null:
		print("player exited but interaction is null")
		return
	info_box.visible = false
	body.clear_interaction(interaction)
