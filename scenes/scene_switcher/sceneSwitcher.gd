extends Node

var current_scene: Node = null
var overworld_scene: Node2D = null

func _ready() -> void:
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() - 1)
	if current_scene is Overworld:
		overworld_scene = current_scene

func goto_overworld() -> void:
	call_deferred("_deferred_goto_overworld")

func _deferred_goto_overworld() -> void:
	current_scene.free()
	current_scene = overworld_scene
	get_tree().root.add_child(overworld_scene)
	get_tree().current_scene = current_scene

func goto_event_scene(path) -> void:
	call_deferred("_deferred_goto_event_scene", path)

func _deferred_goto_event_scene(path) -> void:
	get_tree().root.remove_child(current_scene) #should be same as overworld scene

	var s = ResourceLoader.load(path)
	current_scene = s.instantiate()

	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene
