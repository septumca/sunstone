## Sunstone

# Game summary

Sunstone is action adventure game where user playing as knight trying to win 
heart of princess by completing various tasks the medieval world

# Inspirations

**Moonstone**

Moonstone provides inspiration in general gameplay, i.e. player wandering around
in map with places to explore and to fight monsters. With some areas where
players can upgrade theirs equipment and do other activities other than fighting
enemies

# Player experience

Player will move on overworld map with points of interests. When players come
close to this point, they could enter and do the related fight/activity. After
fight, player will be rewarded with gold or equipment they can use to upgrade
their gear or skills.

# Gameplay

**Map**

Player can move on overworld map, which will be tilemap with different biomes.
There will be no obstacles. Biomes will tell which events can be spawned within.
When player come close to event, UI tooltip will be displayed with short info
about the event with possibility to enter

**Fight area**

Fight area is place where players will fight enemies and at the end will be 
rewarded with gold and/or equipment. This area will have some cosmetic graphics 
(e.g. bones, dirt or grass), but no obstacles Player and monsters can move in 
all  directions, but attack only on horizontal plane. Player can use standard 
attack, heavy attack or dodge. DIfferent monsters will have different means of 
attack, e.g. some of them can only attack, some of them can also dodge or use 
secondary attack. Monsters will be spawned at the start of fight and then 
gradually until the spawn-limit is reached. After all monsters are finished, 
chest or some container at the edge of arena can be interacted with and the UI 
will be presented to player displaying rewards.

**Social area**

Social area is populated with friendly NPCs, with which can player interact and
buy new equipment, exchange items or can get some info about the world. This 
area will be represented by tilemap with houses or other obstacles

# Graphics

Graphics will be stylizied with 1bit colors. Animations in generall will 
be very basic, i.e. wobble effect for move and 1 frame animation from action.
E.g attack will consist of 1 frame as windup, 1 frame hit and 1 frame winddown

# Music

No idea, but very simple. Simple sound effects for different actions, and single
music track per area.

# Controls

Game will be controller only with keyboard. Four keys to move, one to 
attack/interact, when holding will do heavy attack and one key to dodge.
Alternatively mouse or touch can be used, using one click for everything. E.g.
click to move, click on player to attack, hold on player to do heavy attack and
double click to dodge.

# Platforms

Target platforms are Web, Desktop (winodws and linux) and possibly mobile 
(Android)

# Used programs

**Engine**

Godot 4.3 will be used as main engine (possible to upgrade to 4.4 for hopefully
smaller web builds)

https://godotengine.org/

**Graphics**

Asperite (or Krita?), simple rotoscoping, e.g. poses will be taken as picture 
and then traced in either software. Sprite size would be 32x32

https://krita.org/en/
https://www.aseprite.org/

**Music/Sounds**

Famistudio looks good, question is if I will be able to use it?

https://famistudio.org/

# List of game mechanics/events/enemies



TODO
